using Microsoft.AspNetCore.Mvc;

namespace Alexa.Controllers
{
    [Route("api/[controller]")]
    public class AlexaController : Controller
    {
        [HttpPost]
        public dynamic Dan(dynamic request)
        {
            return new
            {
                version = "1.0",
                sessionAttributes = new { },
                response = new
                {
                    outputSpeech = new
                    {
                        type = "PlainText",
                        text = "Dan says Hello back"
                    },
                    card = new
                    {
                        type = "Simple",
                        title = "Dan's Example Skill",
                        content = "Hello Dan"
                    },
                    shouldEndSession = true
                }
            };
        }
    }
}
